package pl.umcs;

import java.util.Objects;

public class TennisGameScore {
    private Integer firstPlayerScore;
    private Integer secondPlayerScore;


    public TennisGameScore(Integer firstPlayerScore, Integer secondPlayerScore) {
        this.firstPlayerScore = firstPlayerScore;
        this.secondPlayerScore = secondPlayerScore;
    }

    public TennisGameScore firstPlayerWonThePoint(){
        return new TennisGameScore(firstPlayerScore +1, secondPlayerScore);
    }


    public TennisGameScore secondPlayerWonThePoint(){
        return new TennisGameScore(firstPlayerScore, secondPlayerScore + 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TennisGameScore that = (TennisGameScore) o;
        return Objects.equals(firstPlayerScore, that.firstPlayerScore) &&
                Objects.equals(secondPlayerScore, that.secondPlayerScore);
    }

    @Override
    public int hashCode() {

        return Objects.hash(firstPlayerScore, secondPlayerScore);
    }

    @Override
    public String toString() {
        return "TennisGameScore{" +
                "firstPlayerScore=" + firstPlayerScore +
                ", secondPlayerScore=" + secondPlayerScore +
                '}';
    }
}
